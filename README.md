# Phone Book application

hackajob challenge

## Requirements

Build a RESTful API for a phone book application. The APIs need to support an authentication method in order to secure your requests.  

 - mandatory endpoints: create, read, update, delete (feel free to extend these endpoints) 
 - use an authentication method to secure your requests (Examples: JWT token, oAuth, etc.) 
 - use a way to make your data persistent (database is preferred) 
 - write at least one unit test and a functional test

## Solution

### Technologies used

 - Java 11
 - Spring Boot
    - Spring MVC
    - Spring Data JPA (with hibernate)
    - Spring Security
 - JWT for authentication
 - H2 embedded database
 - Spring Fox (for swagger and swagger UI)
 - JUnit, Mockito
 - Maven

## Instructions

This project requires JDK 11 or later

Download JDK 11 [http://jdk.java.net/11](http://jdk.java.net/11), extract the archive, set `JAVA_HOME` enviroment variable to `/full/path/to/jdk-11`

### Build the project

building also executes tests

*(use `./mvnw` for Linux and Unix, `./mvnw.cmd` for Windows)*

```sh
./mvnw install
```

### Run from command line

```sh
./mvnw spring-boot:run
```

### Run from IDE

Run the main method in `ludo.phonebook.PhoneBookApplication`, please note this project uses [project lombok](https://projectlombok.org/) to get rid off some of the boilerplate code, therefore working with the code in IntellijIDEA requires a plugin to be installed.
```
File -> Settings -> Plugins -> Browse repositories -> search for "lombok plugin" -> Install
```

## Documentation

There are two kinds of endpoints in the project:

 - starting with `/auth/` prefix - used for register/login of users
 - starting with `/api/` prefix - used for accessing the phone book itself

All endpoints starting with `/api/` requires a token in `authentication` header. In order to obtain the token, one must register and then login (or use `admin@admin.com` as email and `password` as password).

Example scenario:

Register:

```sh
curl 'http://localhost:8080/auth/register' -X POST -d '{"email":"my@email.com", "password":"secret"}' -H 'content-type: application/json'
```

response:

```json
{
   "message" : "successfully registered",
   "success" : true
}
```

Login:

```sh
curl 'http://localhost:8080/auth/login' -X POST -d '{"email":"my@email.com", "password":"secret"}' -H 'content-type: application/json'
```

response:

```json
{
   "success" : true,
   "token" : "eyJhbGciOiJIUzM4NCJ9.eyJ1c2VybmFtZSI6Im15QGVtYWlsLmNvbSJ9.5aOqkgKz7DjncJq9iX8wo7znRGfT3rHUPvQSfy-QNCfkMV1uueDRDY5rI_ldK0eo"
}

```

Search for people with name starting with `c` (case insensitive):

```sh
curl 'http://localhost:8080/api/search?name=c' -H 'authentication: eyJhbGciOiJIUzM4NCJ9.eyJ1c2VybmFtZSI6Im15QGVtYWlsLmNvbSJ9.5aOqkgKz7DjncJq9iX8wo7znRGfT3rHUPvQSfy-QNCfkMV1uueDRDY5rI_ldK0eo'
```

response:

```json
[
   {
      "name" : "Caron Harbert",
      "id" : 5,
      "lastUpdate" : "2018-03-29T12:07:24",
      "phoneNumbers" : [
         {
            "number" : "300-236-4033",
            "id" : 8,
            "note" : "Work"
         },
         {
            "number" : "964-120-6303",
            "id" : 10
         },
         {
            "number" : "817-382-9591",
            "id" : 28,
            "note" : "Office"
         },
         {
            "id" : 33,
            "number" : "660-580-0921",
            "note" : "Work"
         },
         {
            "id" : 38,
            "number" : "778-321-4664"
         },
         {
            "note" : "Personal",
            "id" : 39,
            "number" : "441-403-0052"
         }
      ]
   },
   {
      "id" : 4,
      "name" : "Cloe Joyson",
      "phoneNumbers" : [
         {
            "note" : "Work",
            "id" : 1,
            "number" : "549-151-6443"
         },
         {
            "id" : 21,
            "number" : "666-221-3457",
            "note" : "Office"
         },
         {
            "number" : "873-207-4155",
            "id" : 40,
            "note" : "Office"
         }
      ],
      "lastUpdate" : "2018-04-16T15:23:40"
   }
]
```

For full list of endpoints please see Swagger generated documentation at `http://localhost:8080/swagger-ui.html`.
