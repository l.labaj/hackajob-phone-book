package ludo.phonebook.rest;

import ludo.phonebook.rest.entity.UserRequest;
import ludo.phonebook.security.JwtUtils;
import ludo.phonebook.security.UserEntity;
import ludo.phonebook.security.UserRepository;
import org.junit.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserControllerTest {

    @Test
    public void register() {
        var userRepository = mock(UserRepository.class);
        var passwordEncoder = mock(PasswordEncoder.class);
        var jwtUtils = mock(JwtUtils.class);

        var controller = new UserController(userRepository, passwordEncoder, jwtUtils);

        var userRequest = new UserRequest()
                .setEmail("some@email.com")
                .setPassword("some-password");

        var response = controller.register(userRequest);

        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertNotNull(response.getBody());
        assertTrue(response.getBody().isSuccess());
    }

    @Test
    public void login() {
        var userRepository = mock(UserRepository.class);
        var passwordEncoder = mock(PasswordEncoder.class);
        var jwtUtils = mock(JwtUtils.class);

        when(userRepository.findByEmail("some@email.com"))
                .thenReturn(Optional.of(new UserEntity()
                        .setEmail("some@email.com")
                        .setPassword("encrypted-password")
                ));

        when(passwordEncoder.matches("some-password", "encrypted-password"))
                .thenReturn(true);

        when(jwtUtils.generateToken("some@email.com"))
                .thenReturn("some-token");

        var controller = new UserController(userRepository, passwordEncoder, jwtUtils);

        var userRequest = new UserRequest()
                .setEmail("some@email.com")
                .setPassword("some-password");

        var response = controller.login(userRequest);

        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertNotNull(response.getBody());
        assertTrue(response.getBody().isSuccess());
        assertEquals("some-token", response.getBody().getToken());
    }
}