package ludo.phonebook.rest;

import ludo.phonebook.persistence.PersonRepository;
import ludo.phonebook.persistence.PhoneNumberRepository;
import ludo.phonebook.persistence.entity.Person;
import ludo.phonebook.persistence.entity.PhoneNumber;
import ludo.phonebook.rest.entity.PersonRequest;
import ludo.phonebook.rest.entity.PhoneNumberUpdateRequest;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PhoneBookControllerTest {

    @Test
    public void updatePerson() {
        var personRepository = mock(PersonRepository.class);
        var phoneNumberRepository = mock(PhoneNumberRepository.class);

        when(personRepository.existsById(4L)).thenReturn(true);
        when(personRepository.save(any()))
                .thenReturn(new Person()
                        .setName("new-name")
                        .setId(4L));

        var controller = new PhoneBookController(personRepository, phoneNumberRepository);

        var personRequest = new PersonRequest()
                .setName("new-name");
        var result = controller.updatePerson(personRequest, 4L);

        assertTrue(result.getStatusCode().is2xxSuccessful());
        assertNotNull(result.getBody());
        assertEquals(4L, result.getBody().getId().longValue());
        assertEquals("new-name", result.getBody().getName());
    }

    @Test
    public void updatePersonDoesntExists() {
        var personRepository = mock(PersonRepository.class);
        var phoneNumberRepository = mock(PhoneNumberRepository.class);

        when(personRepository.existsById(4L)).thenReturn(false);
        var controller = new PhoneBookController(personRepository, phoneNumberRepository);

        var personRequest = new PersonRequest()
                .setName("new-name");
        var result = controller.updatePerson(personRequest, 4L);

        assertEquals(404, result.getStatusCodeValue());
    }

    @Test
    public void updatePhoneNumber() {
        var personRepository = mock(PersonRepository.class);
        var phoneNumberRepository = mock(PhoneNumberRepository.class);

        when(phoneNumberRepository.findById(5L))
                .thenReturn(Optional.of(new PhoneNumber()
                        .setId(5L)
                        .setNumber("old-number")
                        .setNote("some-note")));

        when(phoneNumberRepository.save(any()))
                .thenReturn(new PhoneNumber()
                        .setId(5L)
                        .setNumber("new-number")
                        .setNote("some-note"));

        var controller = new PhoneBookController(personRepository, phoneNumberRepository);

        var phoneNumberRequest = new PhoneNumberUpdateRequest()
                .setNumber("new-number")
                .setNote(null);

        var result = controller.updatePhoneNumber(phoneNumberRequest, 5L);

        assertTrue(result.getStatusCode().is2xxSuccessful());
        assertNotNull(result.getBody());
        assertEquals(5L, result.getBody().getId().longValue());
        assertEquals("new-number", result.getBody().getNumber());
        assertEquals("some-note", result.getBody().getNote());
    }

    @Test
    public void search() {
        var personRepository = mock(PersonRepository.class);
        var phoneNumberRepository = mock(PhoneNumberRepository.class);

        var people = List.of(
                new Person().setId(1L).setName("a one"),
                new Person().setId(2L).setName("a two")
        );
        when(personRepository.findByNameStartingWithIgnoreCaseOrderByNameAsc("a"))
                .thenReturn(people);

        var controller = new PhoneBookController(personRepository, phoneNumberRepository);
        var result = controller.search("a");

        assertTrue(result.getStatusCode().is2xxSuccessful());
        assertNotNull(result.getBody());
        assertEquals(1L, result.getBody().get(0).getId().longValue());
        assertEquals("a one", result.getBody().get(0).getName());
        assertEquals(2L, result.getBody().get(1).getId().longValue());
        assertEquals("a two", result.getBody().get(1).getName());
    }
}