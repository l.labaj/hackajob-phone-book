package ludo.phonebook.integration;

import ludo.phonebook.persistence.PhoneNumberRepository;
import ludo.phonebook.persistence.entity.PhoneNumber;
import ludo.phonebook.security.JwtUtils;
import ludo.phonebook.security.UserEntity;
import ludo.phonebook.security.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PhoneNumberTests {
    @Autowired
    private WebApplicationContext context;

    @MockBean
    private PhoneNumberRepository phoneNumberRepository;

    @MockBean
    private JwtUtils jwtUtils;

    @MockBean
    private UserService userService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void phoneNumberUnauthenticated() throws Exception {
        mockMvc.perform(get("/api/phone-number"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void phoneNumberAuthenticated() throws Exception {
        var phoneNumber = new PhoneNumber()
                .setId(5L)
                .setNote("note")
                .setNumber("number");
        when(phoneNumberRepository.findById(any()))
                .thenReturn(Optional.of(phoneNumber));

        when(jwtUtils.extractUsername("some-token"))
                .thenReturn(Optional.of("some@email.com"));

        when(userService.loadUserByUsername("some@email.com"))
                .thenReturn(new UserEntity()
                        .setEmail("some@email.com")
                        .setEnabled(true));

        mockMvc.perform(get("/api/phone-number/5")
                .header("authentication", "some-token"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("id").value(5L))
                .andExpect(jsonPath("note").value("note"))
                .andExpect(jsonPath("number").value("number"));
    }
}
