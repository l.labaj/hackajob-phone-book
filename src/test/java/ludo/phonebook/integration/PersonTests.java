package ludo.phonebook.integration;

import ludo.phonebook.persistence.PersonRepository;
import ludo.phonebook.persistence.entity.Person;
import ludo.phonebook.security.JwtUtils;
import ludo.phonebook.security.UserEntity;
import ludo.phonebook.security.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonTests {

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private PersonRepository personRepository;

    @MockBean
    private JwtUtils jwtUtils;

    @MockBean
    private UserService userService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void personUnauthenticated() throws Exception {
        mockMvc.perform(get("/api/person"))
                .andExpect(status().isForbidden());
    }

    @Test
    public void personAuthenticated() throws Exception {
        var person = new Person()
                .setId(5L)
                .setName("name");

        when(personRepository.findById(any()))
                .thenReturn(Optional.of(person));

        when(jwtUtils.extractUsername("some-token"))
                .thenReturn(Optional.of("some@email.com"));

        when(userService.loadUserByUsername("some@email.com"))
                .thenReturn(new UserEntity()
                        .setEmail("some@email.com")
                        .setEnabled(true));

        mockMvc.perform(get("/api/person/5")
                .header("authentication", "some-token"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("id").value(5L))
                .andExpect(jsonPath("name").value("name"));
    }
}
