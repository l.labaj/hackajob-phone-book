package ludo.phonebook.persistence.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Accessors(chain = true)
public class PhoneNumber {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    private String note;

    private String number;
}
