package ludo.phonebook.persistence;

import ludo.phonebook.persistence.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonRepository extends JpaRepository<Person, Long> {
    List<Person> findByNameStartingWithIgnoreCaseOrderByNameAsc(String name);
}
