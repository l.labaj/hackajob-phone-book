package ludo.phonebook.security;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Optional;

@Component
public class JwtUtils {

    private final Key key;

    public JwtUtils(@Value("${jwt.secret}") String secret) {
        this.key = Keys.hmacShaKeyFor(secret.getBytes());
    }

    public Optional<String> extractUsername(String token) {
        try {
            var username = Jwts.parser()
                    .setSigningKey(key)
                    .parseClaimsJws(token)
                    .getBody()
                    .get("username", String.class);
            return Optional.of(username);
        } catch (JwtException e) {
            return Optional.empty();
        }
    }

    public String generateToken(String username) {

        return Jwts.builder()
                .claim("username", username)
                .signWith(key)
                .compact();
    }
}
