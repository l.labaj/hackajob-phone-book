package ludo.phonebook;

import ludo.phonebook.security.UserEntity;
import ludo.phonebook.security.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Initializer implements ApplicationRunner {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        userService.storeUser(new UserEntity()
                .setEmail("admin@admin.com")
                .setEnabled(true)
                .setPassword(passwordEncoder.encode("password"))
                .setAuthorities(List.of(new SimpleGrantedAuthority("USER"), new SimpleGrantedAuthority("ADMIN")))
        );

    }
}
