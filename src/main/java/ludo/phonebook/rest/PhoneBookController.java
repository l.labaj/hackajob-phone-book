package ludo.phonebook.rest;

import ludo.phonebook.persistence.PersonRepository;
import ludo.phonebook.persistence.PhoneNumberRepository;
import ludo.phonebook.persistence.entity.Person;
import ludo.phonebook.persistence.entity.PhoneNumber;
import ludo.phonebook.rest.entity.PersonRequest;
import ludo.phonebook.rest.entity.PhoneNumberInsertRequest;
import ludo.phonebook.rest.entity.PhoneNumberUpdateRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(path = "/api/", produces = APPLICATION_JSON_VALUE)
public class PhoneBookController {
    private static final Logger log = LoggerFactory.getLogger(PhoneBookController.class);

    private final PersonRepository personRepository;
    private final PhoneNumberRepository phoneNumberRepository;

    public PhoneBookController(PersonRepository personRepository, PhoneNumberRepository phoneNumberRepository) {
        this.personRepository = personRepository;
        this.phoneNumberRepository = phoneNumberRepository;
    }


    @GetMapping("/person")
    public ResponseEntity<List<Person>> allPeople() {
        return ok(personRepository.findAll());
    }

    @GetMapping("/person/{id}")
    public ResponseEntity<Person> getOnePerson(@PathVariable long id) {
        log.info("finding person with id {}", id);

        return personRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElse(notFound().build());
    }

    @PostMapping("/person/insert")
    public ResponseEntity<Person> insertPerson(@Valid @RequestBody PersonRequest body) {
        log.info("inserting person '{}'", body);

        var entity = new Person()
                .setName(body.getName())
                .setLastUpdate(LocalDateTime.now());

        var persisted = personRepository.save(entity);

        return ok(persisted);
    }

    @DeleteMapping("/person/{id}")
    public ResponseEntity<Person> deletePerson(@PathVariable long id) {
        log.info("deleting person with id {}", id);

        var person = personRepository.findById(id);
        if (person.isPresent()) {
            personRepository.deleteById(id);
            return ok(person.get());
        } else {
            return notFound().build();
        }
    }

    @PostMapping("/person/{id}")
    public ResponseEntity<Person> updatePerson(@Valid @RequestBody PersonRequest body, @PathVariable long id) {
        log.info("updating person id {} with '{}'", id, body);

        if (personRepository.existsById(id)) {
            var entity = new Person()
                    .setName(body.getName())
                    .setId(id)
                    .setLastUpdate(LocalDateTime.now());
            var persisted = personRepository.save(entity);
            return ok(persisted);
        } else {
            return notFound().build();
        }
    }


    @GetMapping("/phone-number")
    public ResponseEntity<List<PhoneNumber>> allPhoneNumbers() {
        return ok(phoneNumberRepository.findAll());
    }

    @GetMapping("/phone-number/{id}")
    public ResponseEntity<PhoneNumber> getOnePhoneNumber(@PathVariable long id) {
        log.info("finding phone number with id {}", id);

        return phoneNumberRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElse(notFound().build());
    }

    @PostMapping("/phone-number/insert")
    public ResponseEntity<PhoneNumber> insertPhoneNumber(@Valid @RequestBody PhoneNumberInsertRequest body) {
        log.info("inserting phone number '{}'", body);

        var person = personRepository.findById(body.getPersonId());
        if (person.isPresent()) {
            var entity = new PhoneNumber()
                    .setNumber(body.getNumber())
                    .setNote(body.getNote());
            var persisted = phoneNumberRepository.save(entity);
            return ok(persisted);
        } else {
            return notFound().build();
        }
    }

    @DeleteMapping("/phone-number/{id}")
    public ResponseEntity<PhoneNumber> deletePhoneNumber(@PathVariable long id) {
        log.info("deleting phone number with id {}", id);

        var phoneNumber = phoneNumberRepository.findById(id);
        if (phoneNumber.isPresent()) {
            phoneNumberRepository.deleteById(id);
            return ok(phoneNumber.get());
        } else {
            return notFound().build();
        }
    }

    @PostMapping("/phone-number/{id}")
    public ResponseEntity<PhoneNumber> updatePhoneNumber(@Valid @RequestBody PhoneNumberUpdateRequest body, @PathVariable long id) {
        log.info("updating phone number id {} with '{}'", id, body);

        return phoneNumberRepository.findById(id)
                .map(loaded -> {
                    var entity = new PhoneNumber()
                            .setId(loaded.getId())
                            .setNote(loaded.getNote())
                            .setNumber(loaded.getNumber());

                    if (body.getNote() != null) {
                        entity.setNote(body.getNote());
                    }
                    if (body.getNumber() != null) {
                        entity.setNumber(body.getNumber());
                    }
                    return phoneNumberRepository.save(entity);
                })
                .map(ResponseEntity::ok)
                .orElseGet(() -> notFound().build());
    }

    @GetMapping("/search")
    public ResponseEntity<List<Person>> search(@RequestParam String name) {
        log.info("search query for '{}'", name);

        var result = personRepository.findByNameStartingWithIgnoreCaseOrderByNameAsc(name);
        return ok(result);
    }
}
