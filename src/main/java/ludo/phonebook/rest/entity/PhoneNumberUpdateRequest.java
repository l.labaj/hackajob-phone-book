package ludo.phonebook.rest.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PhoneNumberUpdateRequest {
    private String number;
    private String note;
}
