package ludo.phonebook.rest.entity;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class PhoneNumberInsertRequest {
    private long personId;

    @NotBlank
    private String number;

    private String note;

}
