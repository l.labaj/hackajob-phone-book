package ludo.phonebook.rest.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
public class PersonRequest {
    @NotBlank
    private String name;
}
