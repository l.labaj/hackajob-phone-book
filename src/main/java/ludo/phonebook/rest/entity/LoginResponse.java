package ludo.phonebook.rest.entity;

import lombok.Value;

@Value
public class LoginResponse {
    String token;
    boolean success;
    String message;
}
