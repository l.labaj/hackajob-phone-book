package ludo.phonebook.rest.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
@Accessors(chain = true)
public class UserRequest {
    @Email
    private String email;

    @Size(min = 6, max = 32)
    private String password;
}
