package ludo.phonebook.rest.entity;

import lombok.Value;

@Value
public class RegisterResponse {
    String message;
    boolean success;
}
