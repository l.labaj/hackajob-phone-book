package ludo.phonebook.rest;

import ludo.phonebook.rest.entity.LoginResponse;
import ludo.phonebook.rest.entity.RegisterResponse;
import ludo.phonebook.rest.entity.UserRequest;
import ludo.phonebook.security.JwtUtils;
import ludo.phonebook.security.UserEntity;
import ludo.phonebook.security.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(path = "/auth", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
public class UserController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtils jwtUtils;

    public UserController(UserRepository userRepository, PasswordEncoder passwordEncoder, JwtUtils jwtUtils) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtUtils = jwtUtils;
    }

    @PostMapping("/register")
    public ResponseEntity<RegisterResponse> register(@Valid @RequestBody UserRequest body) {
        log.info("registering '{}'", body.getEmail());
        if (userRepository.existsByEmail(body.getEmail())) {
            return badRequest()
                    .body(new RegisterResponse("email already in use", false));
        } else {
            var entity = new UserEntity()
                    .setEmail(body.getEmail())
                    .setAuthorities(List.of(new SimpleGrantedAuthority("USER")))
                    .setEnabled(true)
                    .setPassword(passwordEncoder.encode(body.getPassword()));
            userRepository.save(entity);
            return ok(new RegisterResponse("successfully registered", true));
        }
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@Valid @RequestBody UserRequest body) {
        log.info("logging in '{}'", body.getEmail());

        var user = userRepository.findByEmail(body.getEmail());
        if (user.isPresent()) {
            var matches = passwordEncoder.matches(body.getPassword(), user.get().getPassword());
            if (matches) {
                var token = jwtUtils.generateToken(user.get().getUsername());
                return ok(new LoginResponse(token, true, null));
            } else {
                return badRequest().body(new LoginResponse(null, false, "invalid email or password"));
            }
        } else {
            return badRequest().body(new LoginResponse(null, false, "invalid email or password"));
        }
    }
}
